

function dogFactory(dogName){
    
    
    var isHappy = true;
    
    
    function dogBark(){

        if(isHappy)
        {
                    console.log('WOOF!');
        }
        else{
                    console.log('woooof...');
        }
        
    }
    
    function dogEat(food){
        
        if( food === 'grass'){
            isHappy = false;
        }
        
    }
        
    
    return {
        
        name: dogName,
        bark: dogBark,
        eat : dogEat
            
        
    };
    
    
    
}


