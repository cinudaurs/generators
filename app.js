




function apiCall(time, string){
    
    var res = document.getElementById('results');
    
    var payload = {
        string: string,
        timeout: time
    }
    
    //simulate the latency of the http call.
    setTimeout(function(){gen.next(payload)}, time);
        
    }


function* myGenerator(){
    
    var res = document.getElementById('results');
    
    var firstApiCall = yield apiCall(1000, 'test');
    res.innerHTML += ('First Api Call -> Latency: ' + firstApiCall.timeout + ' Data : '+ firstApiCall.string + '<br>');
    
    var secondApiCall = yield apiCall(3200, 'Second call, has first call data');
    res.innerHTML += ('Second Api Call -> Latency: ' + secondApiCall.timeout + ' Data : '+ secondApiCall.string + ' -> First call data :' + firstApiCall.string + '<br>' );
    
    var thirdApiCall = yield apiCall(4500, 'Third call, has first and second call data');
    res.innerHTML += ('Third Api Call -> Latency: ' + thirdApiCall.timeout + ' Data : '+ secondApiCall.string + ' -> First call data :' + firstApiCall.string + ' -> Second call data' + secondApiCall.string + '<br>' );
}


//function* myGenerator(){
//    
//    var res = document.getElementById('results');
//    
//    var firstLanguage = yield 'First Language';
//    var secondLanguage = yield 'Second Language';
//    var age = yield 'age';    
//    
//    var ageDiff = age + 10;
//     
//    res.innerHTML += ('first language: ' + firstLanguage + ' <br> second language: '+ secondLanguage + '<br> age: '+ age+ '<br> In ten years you will be :'+ ageDiff + '<br>');
//     
//}


var gen = myGenerator();

//function doGen(num){
//    
//    var res = document.getElementById('results');
//    
//    for(i=0; i< num; i++){
//        
//        var yielded = gen.next().value;
//        
//        res.innerHTML += (yielded + '<br>');
//        
//    }
//    
//}

function doGen(){
    
    var res = document.getElementById('results');
    
    var first = gen.next().value;
//    res.innerHTML += (first + '<br>');
    
  //  var second = gen.next().value;
    
//    var second = gen.next('javascript').value;
//    res.innerHTML += (second + '<br>');
//    
//    var third = gen.next('ruby').value;
//    res.innerHTML += (third + '<br>');
//    
//    var fourth = gen.next(30).done;
//    res.innerHTML += (fourth + '<br>');
    
}



