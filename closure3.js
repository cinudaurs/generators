(function(){
    
    var btns = document.querySelectorAll('.btn');
    
    function alertNum(num){
            
            alert(num);
            
        }
    
    
    for (var i=0; i<btns.length; i++){
        
        var btn = btns[i];
        
        //uncomment #1
//        btn.addEventListener('click', function(){
//            alert(i);       //clicking will give a value of 6 for all doors. this can be fixed by IIFE as below.
//        });
        
        
        //comment#1, uncomment #2
        // using IIFE
//        (function(icaptured){
//         
//        btn.addEventListener('click', function(){
//            alert(icaptured);
//        });    
//            
//        })(i);
        
        
        //comment #2, uncomment #3
//        btn.addEventListener('click', alertNum.bind(this, i));      
        
        
        function makeAlertNumFunction(num){
            
            return function(){
                
                alert(num);
                
            };
            
        }
        
        
        
        //comment #3, uncomment #4
        btn.addEventListener('click', makeAlertNumFunction(i)); //makeAlertNumFunction() runs when the evenListener is being created, tht's why we parameterize and pass i, this parameter gets locked in as the                                                             // return parameter of makeAlertNumFunction above. everyone of the buttons have each a function that returns the num passed.
        
        
        
        
        
    }
    
    
    
    
    
    
})();